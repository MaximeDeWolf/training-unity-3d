using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MovementController : MonoBehaviour
{
    [Header("Reference")]
    [Tooltip("The game object of the parent")]
    [SerializeField]
    private GameObject parentGameObject;
    [Tooltip("Th transform of the main camera")]
    [SerializeField]
    private Transform cameraTransform;

    [Header("Movement")]
    [Tooltip("The movement speed of the hero in unit/sec")]
    [SerializeField]
    private float walkSpeed = 1;
    [Tooltip("The force of the hero's jump")]
    [SerializeField]
    private float jumpImpulse = 10;
    [Tooltip("The layer mask of the ground to check if the hero can jump")]
    [SerializeField]
    private LayerMask groundMask;
    [Tooltip("The time the rotation take to totally complete")]
    [SerializeField]
    private float rotationTime = 0.1f;

    private Vector2 movement;
    private Vector3 jumpDirection = new Vector3(0, 0, 0);
    float currentAngleVelocity;

    private Rigidbody heroRigidbody;
    private CapsuleCollider heroCollider;

    private void Awake()
    {
        heroRigidbody = parentGameObject.GetComponent<Rigidbody>();
        heroCollider = GetComponent<CapsuleCollider>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        Walk();
        LookTowardsWalkDirection();
        Jump();
    }

    /// <summary>
    /// Update the hero movement information
    /// </summary>
    /// <param name="context">The value returned by the player Input System</param>
    public void OnWalk(InputAction.CallbackContext context)
    {
        movement = context.ReadValue<Vector2>();
    }

    /// <summary>
    /// Update the hero jump info. Jump 2 times further is the utton is hold.
    /// </summary>
    /// <param name="context">The value returned by the player Input System</param>
    public void OnJump(InputAction.CallbackContext context)
    {
        Vector3 direction = Vector3.up;
        if (context.performed){
            jumpDirection = direction * jumpImpulse * 2;
        } else if (context.canceled)
        {
            jumpDirection = direction * jumpImpulse;
        }
    }

    /// <summary>
    /// Return true iif the hero touch the ground (can jump)
    /// </summary>
    /// <returns> Return true iif the hero touch the ground (can jump)</returns>
    private bool IsGrounded()
    {
        RaycastHit hit;
        float offset = 0.1f;
        return Physics.SphereCast(heroCollider.bounds.center, heroCollider.radius + offset, Vector3.down, out hit, heroCollider.radius, groundMask);
    }

    /// <summary>
    /// Make the hero walks according to the controller input
    /// </summary>
    private void Walk()
    {
        // To Be sure there no movement when 0 input because the "targetAngle" is never really 0
        if(!movement.Equals(new Vector2(0, 0)))
        {
            //Take into account the angle of the camera to move accordingly
            float targetAngle = Mathf.Atan2(movement.x, movement.y) * Mathf.Rad2Deg + cameraTransform.eulerAngles.y;
            Vector3 cameraDirection = (Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward).normalized;

            Vector3 distance = cameraDirection * walkSpeed * Time.fixedDeltaTime;
            parentGameObject.transform.Translate(distance.x, 0, distance.z);
        }
            
    }

    /// <summary>
    /// Rotate the player so he look towards his walking direction
    /// The rotation is made smoothly.
    /// </summary>
    private void LookTowardsWalkDirection()
    {
        float targetAngle = Mathf.Atan2(movement.x, movement.y) * Mathf.Rad2Deg + cameraTransform.eulerAngles.y;
        float smoothAngle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref currentAngleVelocity, rotationTime);
        transform.rotation = Quaternion.Euler(0f, smoothAngle, 0f);
    }

    /// <summary>
    /// Make the hero jump according to the controller input.
    /// </summary>
    private void Jump()
    {
        if ( !jumpDirection.Equals(new Vector3(0, 0, 0)) && IsGrounded())
        {
            heroRigidbody.AddForce(jumpDirection, ForceMode.Impulse);
            jumpDirection = new Vector3(0, 0, 0);
        }
    }
}

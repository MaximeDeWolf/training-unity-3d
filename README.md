# Training Unity 3D

The project aims to train me with several features that can be implemented in Unity3D such as:
- [X] camera movement with Cinemachine
- [ ] basic 3d animation
- [ ] basic interaction with the scene
- [X] build a basic level
- [X] use basic controls
- [ ] minimaps
- [ ] shaders
- [ ] custom Unity editor
- [ ] ...
